﻿namespace Otus.Teaching.Pcf.Common.DataAccess.Helpers.Base;

public interface IDbInitializer
{
    void InitializeDb();
}