﻿using System;
using System.Threading.Tasks;
using MassTransit;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.WebHost.Services.Base;
using Otus.Teaching.Pcf.Common.Integration.Models;

namespace Otus.Teaching.Pcf.Administration.WebHost.Consumers;

public class EmployeeIdConsumer(IRepository<Employee> employeeRepository, IPromoCodeApplyService promoCodeApplyService) : IConsumer<EmployeeIdModel>
{
    public async Task Consume(ConsumeContext<EmployeeIdModel> context)
    {
        var model = context.Message;
        var employee = await employeeRepository.GetByIdAsync(model.EmployeeId);
        if (employee == null)
            throw new InvalidOperationException("Employee does not exists");
        await promoCodeApplyService.UpdateAppliedPromoCodesAsync(employee);
    }
}