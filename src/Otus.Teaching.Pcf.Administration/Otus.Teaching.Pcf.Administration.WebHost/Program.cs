using System.Reflection;
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.DataAccess;
using Otus.Teaching.Pcf.Administration.DataAccess.Data;
using Otus.Teaching.Pcf.Administration.DataAccess.Repositories;
using Otus.Teaching.Pcf.Administration.WebHost.Consumers;
using Otus.Teaching.Pcf.Administration.WebHost.Services;
using Otus.Teaching.Pcf.Administration.WebHost.Services.Base;
using Otus.Teaching.Pcf.Common.DataAccess.Helpers.Base;
using Otus.Teaching.Pcf.Common.Database.Helpers;
using Otus.Teaching.Pcf.Common.MassTransit.RabbitMq.Configuration.Helpers;

var builder = WebApplication.CreateBuilder(args);
builder.Configuration.AddEnvironmentVariables();
builder.Services.AddControllers().AddMvcOptions(x =>
    x.SuppressAsyncSuffixInActionNames = false);
builder.Services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
builder.Services.AddScoped<IDbInitializer, EfDbInitializer>();
builder.Services.AddScoped<IPromoCodeApplyService, PromoCodeApplyService>();
builder.Configuration.AddEnvironmentVariables();
builder.Services.AddDbContext<DataContext>(x =>
{
    //x.UseSqlite("Filename=PromocodeFactoryAdministrationDb.sqlite");
    x.UseNpgsql(builder.Configuration.GetConnectionString("PromocodeFactoryAdministrationDb"));
    x.UseSnakeCaseNamingConvention();
    x.UseLazyLoadingProxies();
});

builder.Services.AddOpenApiDocument(options =>
{
    options.Title = "PromoCode Factory Administration API Doc";
    options.Version = "1.0";
});
builder.Services.AddMassTransitRabbitMqConfiguration(builder.Configuration);
builder.Services.AddMassTransit(configurator =>
{
    configurator.AddConsumer<EmployeeIdConsumer>();
    configurator.UsingRabbitMq((context, configurator) =>
    {
        configurator.ConfigureEndpoints(context);
    });
});
var app = builder.Build();
if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}
else
{
    app.UseHsts();
}

app.UseOpenApi();
app.UseSwaggerUi(settings => { settings.DocExpansion = "list"; });

app.UseHttpsRedirection();

app.UseRouting();


app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");
app.MigrateDatabase<DataContext>();
app.InitializeDatabase();
app.Run();

