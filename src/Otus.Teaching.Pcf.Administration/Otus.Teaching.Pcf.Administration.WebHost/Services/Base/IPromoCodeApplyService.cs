﻿using System;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.WebHost.Services.Base;

public interface IPromoCodeApplyService
{
    Task UpdateAppliedPromoCodesAsync(Employee employee);
}