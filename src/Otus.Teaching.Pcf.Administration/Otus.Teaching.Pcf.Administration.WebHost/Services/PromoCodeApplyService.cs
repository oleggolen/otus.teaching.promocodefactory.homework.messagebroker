﻿using System.Threading.Tasks;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.WebHost.Services.Base;

namespace Otus.Teaching.Pcf.Administration.WebHost.Services;

public class PromoCodeApplyService(IRepository<Employee> employeeRepository) : IPromoCodeApplyService
{
    public async Task UpdateAppliedPromoCodesAsync(Employee employee)
    {
        employee.AppliedPromocodesCount++;
        await employeeRepository.UpdateAsync(employee);
    }
}