﻿using System.Net.Http;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration;

public class GivingPromoCodeToCustomerGateway
    : IGivingPromoCodeToCustomerGateway
{
    private readonly HttpClient _httpClient;

    public GivingPromoCodeToCustomerGateway(HttpClient httpClient)
    {
        _httpClient = httpClient;
    }

    public async Task GivePromoCodeToCustomer(PromoCode promoCode)
    {


    }
}