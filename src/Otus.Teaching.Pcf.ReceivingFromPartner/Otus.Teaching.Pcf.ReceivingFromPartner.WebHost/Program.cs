using System;
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.Pcf.Common.DataAccess.Helpers.Base;
using Otus.Teaching.Pcf.Common.Database.Helpers;
using Otus.Teaching.Pcf.Common.MassTransit.RabbitMq.Configuration.Helpers;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.ReceivingFromPartner.DataAccess;
using Otus.Teaching.Pcf.ReceivingFromPartner.DataAccess.Data;
using Otus.Teaching.Pcf.ReceivingFromPartner.DataAccess.Repositories;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration;

var builder = WebApplication.CreateBuilder(args); 
builder.Services.AddControllers().AddMvcOptions(x =>
    x.SuppressAsyncSuffixInActionNames = false);
builder.Services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
builder.Services.AddScoped<INotificationGateway, NotificationGateway>();
builder.Services.AddScoped<IDbInitializer, EfDbInitializer>();


builder.Services.AddDbContext<DataContext>(x =>
{
    //x.UseSqlite("Filename=PromocodeFactoryReceivingFromPartnerDb.sqlite");
    x.UseNpgsql(builder.Configuration.GetConnectionString("PromocodeFactoryReceivingFromPartnerDb"));
    x.UseSnakeCaseNamingConvention();
    x.UseLazyLoadingProxies();
});

builder.Services.AddOpenApiDocument(options =>
{
    options.Title = "PromoCode Factory Receiving From Partner API Doc";
    options.Version = "1.0";
});
builder.Services.AddMassTransitRabbitMqConfiguration(builder.Configuration);
builder.Services.AddMassTransit(configurator =>
{
    
    configurator.UsingRabbitMq();
});
var app = builder.Build();
if (app.Environment.IsDevelopment())
    app.UseDeveloperExceptionPage();
else
    app.UseHsts();

app.UseOpenApi();
app.UseSwaggerUi(x => { x.DocExpansion = "list"; });

app.UseHttpsRedirection();

app.UseRouting();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.MigrateDatabase<DataContext>();
app.InitializeDatabase();
app.Run();