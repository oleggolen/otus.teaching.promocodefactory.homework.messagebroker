﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.Common.Integration.Models;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Mappers;
using Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Models;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Controllers;

/// <summary>
///     Партнеры
/// </summary>
[ApiController]
[Route("api/v1/[controller]")]
public class PartnersController(
    IRepository<Partner> partnersRepository,
    IRepository<Preference> preferencesRepository,
    INotificationGateway notificationGateway,
    IPublishEndpoint publishEndpoint)
    : ControllerBase
{
    /// <summary>
    ///     Получить список партнеров
    /// </summary>
    [HttpGet]
    public async Task<ActionResult<List<PartnerResponse>>> GetPartnersAsync()
    {
        var partners = await partnersRepository.GetAllAsync();

        var response = partners.Select(x => new PartnerResponse
        {
            Id = x.Id,
            Name = x.Name,
            NumberIssuedPromoCodes = x.NumberIssuedPromoCodes,
            IsActive = true,
            PartnerLimits = x.PartnerLimits
                .Select(y => new PartnerPromoCodeLimitResponse
                {
                    Id = y.Id,
                    PartnerId = y.PartnerId,
                    Limit = y.Limit,
                    CreateDate = y.CreateDate.ToString(),
                    EndDate = y.EndDate.ToString(),
                    CancelDate = y.CancelDate?.ToString()
                }).ToList()
        });

        return Ok(response);
    }

    /// <summary>
    ///     Получить информацию партнере
    /// </summary>
    /// <param name="id">Id партнера, например:
    ///     <example>20d2d612-db93-4ed5-86b1-ff2413bca655</example>
    /// </param>
    [HttpGet("{id:guid}")]
    public async Task<ActionResult<List<PartnerResponse>>> GetPartnersAsync(Guid id)
    {
        var partner = await partnersRepository.GetByIdAsync(id);

        if (partner == null) return NotFound();

        var response = new PartnerResponse
        {
            Id = partner.Id,
            Name = partner.Name,
            NumberIssuedPromoCodes = partner.NumberIssuedPromoCodes,
            IsActive = true,
            PartnerLimits = partner.PartnerLimits
                .Select(y => new PartnerPromoCodeLimitResponse
                {
                    Id = y.Id,
                    PartnerId = y.PartnerId,
                    Limit = y.Limit,
                    CreateDate = y.CreateDate.ToString(),
                    EndDate = y.EndDate.ToString(),
                    CancelDate = y.CancelDate?.ToString()
                }).ToList()
        };

        return Ok(response);
    }

    /// <summary>
    ///     Установить лимит на промокоды для партнера
    /// </summary>
    [HttpPost("{id:guid}/limits")]
    public async Task<IActionResult> SetPartnerPromoCodeLimitAsync(Guid id, SetPartnerPromoCodeLimitRequest request)
    {
        var partner = await partnersRepository.GetByIdAsync(id);

        if (partner == null)
            return NotFound();

        //Если партнер заблокирован, то нужно выдать исключение
        if (!partner.IsActive)
            return BadRequest("Данный партнер не активен");

        //Установка лимита партнеру
        var activeLimit = partner.PartnerLimits.FirstOrDefault(x =>
            !x.CancelDate.HasValue);

        if (activeLimit != null)
        {
            //Если партнеру выставляется лимит, то мы 
            //должны обнулить количество промокодов, которые партнер выдал, если лимит закончился, 
            //то количество не обнуляется
            partner.NumberIssuedPromoCodes = 0;

            //При установке лимита нужно отключить предыдущий лимит
            activeLimit.CancelDate = DateOnly.FromDateTime(DateTime.Now);
        }

        if (request.Limit <= 0)
            return BadRequest("Лимит должен быть больше 0");

        var newLimit = new PartnerPromoCodeLimit
        {
            Limit = request.Limit,
            Partner = partner,
            PartnerId = partner.Id,
            CreateDate = DateOnly.FromDateTime(DateTime.Now),
            EndDate = DateOnly.FromDateTime(request.EndDate)
        };

        partner.PartnerLimits.Add(newLimit);

        await partnersRepository.UpdateAsync(partner);

        await notificationGateway
            .SendNotificationToPartnerAsync(partner.Id, "Вам установлен лимит на отправку промокодов...");

        return CreatedAtAction(nameof(GetPartnerLimitAsync), new { id = partner.Id, limitId = newLimit.Id }, null);
    }

    /// <summary>
    ///     Получить лимит на промокоды для партнера
    /// </summary>
    /// <param name="id">Id партнера, например:
    ///     <example>20d2d612-db93-4ed5-86b1-ff2413bca655</example>
    /// </param>
    /// <param name="limitId">Id лимита партнера, например:
    ///     <example>93f3a79d-e9f9-47e6-98bb-1f618db43230</example>
    /// </param>
    [HttpGet("{id:guid}/limits/{limitId:guid}")]
    public async Task<ActionResult<PartnerPromoCodeLimit>> GetPartnerLimitAsync(Guid id, Guid limitId)
    {
        var partner = await partnersRepository.GetByIdAsync(id);

        if (partner == null)
            return NotFound();

        var limit = partner.PartnerLimits
            .FirstOrDefault(x => x.Id == limitId);

        var response = new PartnerPromoCodeLimitResponse
        {
            Id = limit.Id,
            PartnerId = limit.PartnerId,
            Limit = limit.Limit,
            CreateDate = limit.CreateDate.ToString(),
            EndDate = limit.EndDate.ToString(),
            CancelDate = limit.CancelDate?.ToString()
        };

        return Ok(response);
    }

    /// <summary>
    ///     Отменить лимит на промокоды для партнера
    /// </summary>
    /// <param name="id">Id партнера, например:
    ///     <example>0da65561-cf56-4942-bff2-22f50cf70d43</example>
    /// </param>
    [HttpPost("{id:guid}/canceledLimits")]
    public async Task<IActionResult> CancelPartnerPromoCodeLimitAsync(Guid id)
    {
        var partner = await partnersRepository.GetByIdAsync(id);

        if (partner == null)
            return NotFound();

        //Если партнер заблокирован, то нужно выдать исключение
        if (!partner.IsActive)
            return BadRequest("Данный партнер не активен");

        //Отключение лимита
        var activeLimit = partner.PartnerLimits.FirstOrDefault(x =>
            !x.CancelDate.HasValue);

        if (activeLimit != null) activeLimit.CancelDate = DateOnly.FromDateTime(DateTime.Now);

        await partnersRepository.UpdateAsync(partner);

        //Отправляем уведомление
        await notificationGateway
            .SendNotificationToPartnerAsync(partner.Id, "Ваш лимит на отправку промокодов отменен...");

        return NoContent();
    }

    /// <summary>
    ///     Получить промокод партнера по id
    /// </summary>
    /// <returns></returns>
    [HttpGet("{id:guid}/promocodes")]
    public async Task<IActionResult> GetPartnerPromoCodesAsync(Guid id)
    {
        var partner = await partnersRepository.GetByIdAsync(id);

        if (partner == null) return NotFound("Партнер не найден");

        var response = partner.PromoCodes
            .Select(x => new PromoCodeShortResponse
            {
                Id = x.Id,
                Code = x.Code,
                BeginDate = x.BeginDate.ToString(CultureInfo.InvariantCulture),
                EndDate = x.EndDate.ToString(CultureInfo.InvariantCulture),
                PartnerName = x.Partner.Name,
                PartnerId = x.PartnerId,
                ServiceInfo = x.ServiceInfo
            }).ToList();

        return Ok(response);
    }

    /// <summary>
    ///     Получить промокод партнера по id
    /// </summary>
    /// <returns></returns>
    [HttpGet("{id:guid}/promocodes/{promoCodeId:guid}")]
    public async Task<IActionResult> GetPartnerPromoCodeAsync(Guid id, Guid promoCodeId)
    {
        var partner = await partnersRepository.GetByIdAsync(id);

        if (partner == null) return NotFound("Партнер не найден");

        var promoCode = partner.PromoCodes.FirstOrDefault(x => x.Id == promoCodeId);

        if (promoCode == null) return NotFound("Партнер не найден");

        var response = new PromoCodeShortResponse
        {
            Id = promoCode.Id,
            Code = promoCode.Code,
            BeginDate = promoCode.BeginDate.ToString(CultureInfo.InvariantCulture),
            EndDate = promoCode.EndDate.ToString(CultureInfo.InvariantCulture),
            PartnerName = promoCode.Partner.Name,
            PartnerId = promoCode.PartnerId,
            ServiceInfo = promoCode.ServiceInfo
        };

        return Ok(response);
    }

    /// <summary>
    ///     Создать промокод от партнера
    /// </summary>
    /// <param name="id">Id партнера, например:
    ///     <example>20d2d612-db93-4ed5-86b1-ff2413bca655</example>
    /// </param>
    /// <param name="request">Данные запроса/example></param>
    /// <returns></returns>
    [HttpPost("{id:guid}/promocodes")]
    public async Task<IActionResult> ReceivePromoCodeFromPartnerWithPreferenceAsync(Guid id,
        ReceivingPromoCodeRequest request)
    {
        var partner = await partnersRepository.GetByIdAsync(id);

        if (partner == null) return BadRequest("Партнер не найден");

        var activeLimit = partner.PartnerLimits.FirstOrDefault(x
            => !x.CancelDate.HasValue && x.EndDate > DateOnly.FromDateTime(DateTime.Now));

        if (activeLimit == null) return BadRequest("Нет доступного лимита на предоставление промокодов");

        if (partner.NumberIssuedPromoCodes + 1 > activeLimit.Limit)
            return BadRequest("Лимит на выдачу промокодов превышен");

        if (partner.PromoCodes.Any(x => x.Code == request.PromoCode))
            return BadRequest("Данный промокод уже был выдан ранее");

        //Получаем предпочтение по имени
        var preference = await preferencesRepository.GetByIdAsync(request.PreferenceId);

        if (preference == null) return BadRequest("Предпочтение не найдено");

        var promoCode = PromoCodeMapper.MapFromModel(request, preference, partner);

        partner.PromoCodes.Add(promoCode);
        partner.NumberIssuedPromoCodes++;

        await partnersRepository.UpdateAsync(partner);
        var dto = new GivePromoCodeToCustomerModel
        {
            PartnerId = promoCode.Partner.Id,
            BeginDate = promoCode.BeginDate.ToShortDateString(),
            EndDate = promoCode.EndDate.ToShortDateString(),
            PreferenceId = promoCode.PreferenceId,
            PromoCode = promoCode.Code,
            ServiceInfo = promoCode.ServiceInfo,
            PartnerManagerId = promoCode.PartnerManagerId
        };

        //TODO: Чтобы информация о том, что промокод был выдан парнером была отправлена
        //в микросервис рассылки клиентам нужно либо вызвать его API, либо отправить событие в очередь
        await publishEndpoint.Publish(dto);

        //TODO: Чтобы информация о том, что промокод был выдан парнером была отправлена
        //в микросервис администрирования нужно либо вызвать его API, либо отправить событие в очередь

        if (request.PartnerManagerId.HasValue)
            await publishEndpoint.Publish(new EmployeeIdModel
            {
                EmployeeId = request.PartnerManagerId.Value
            });

        return CreatedAtAction(nameof(GetPartnerPromoCodeAsync),
            new { id = partner.Id, promoCodeId = promoCode.Id }, null);
    }
}