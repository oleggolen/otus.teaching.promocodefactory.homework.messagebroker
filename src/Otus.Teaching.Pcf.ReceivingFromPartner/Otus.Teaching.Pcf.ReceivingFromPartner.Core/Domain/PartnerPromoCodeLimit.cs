﻿using System;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;

public class
    PartnerPromoCodeLimit
{
    public Guid Id { get; set; }

    public Guid PartnerId { get; set; }

    public virtual Partner Partner { get; set; }

    public DateOnly CreateDate { get; set; }

    public DateOnly? CancelDate { get; set; }

    public DateOnly EndDate { get; set; }

    public int Limit { get; set; }
}