﻿namespace Otus.Teaching.Pcf.Common.Integration.Models;

public class EmployeeIdModel
{
    public Guid EmployeeId { get; init; }
}