﻿using System;
using System.Threading.Tasks;
using MassTransit;
using Otus.Teaching.Pcf.Common.Integration.Models;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services.Base;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Consumers;

public class PromoCodeConsumer(
    IPromoCodeGivingService promoCodeGivingService,
    IRepository<Preference> preferencesRepository)
    : IConsumer<GivePromoCodeToCustomerModel>
{
    public async Task Consume(ConsumeContext<GivePromoCodeToCustomerModel> context)
    {
        var request = context.Message;
        var preference = await preferencesRepository.GetByIdAsync(request.PreferenceId);
        if (preference == null)
            throw new InvalidOperationException("Preference does not exists");
        await promoCodeGivingService.GivePromoCodesToCustomersWithPreferenceAsync(request, preference);
    }
}