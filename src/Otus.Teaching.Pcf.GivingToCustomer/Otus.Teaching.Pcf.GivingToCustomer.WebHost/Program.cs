using System.Reflection;
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.CookiePolicy;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.Common.DataAccess.Helpers.Base;
using Otus.Teaching.Pcf.Common.Database.Helpers;
using Otus.Teaching.Pcf.Common.MassTransit.RabbitMq.Configuration.Helpers;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Integration;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Consumers;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services.Base;

var builder = WebApplication.CreateBuilder(args); 
builder.Services.AddControllers().AddMvcOptions(x =>
    x.SuppressAsyncSuffixInActionNames = false);
builder.Services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
builder.Services.AddScoped<INotificationGateway, NotificationGateway>();
builder.Services.AddScoped<IDbInitializer, EfDbInitializer>();
builder.Services.AddScoped<IPromoCodeGivingService, PromoCodeGivingService>();
builder.Services.AddDbContext<DataContext>(x =>
{
    //x.UseSqlite("Filename=PromocodeFactoryGivingToCustomerDb.sqlite");
    x.UseNpgsql(builder.Configuration.GetConnectionString("PromocodeFactoryGivingToCustomerDb"));
    x.UseSnakeCaseNamingConvention();
    x.UseLazyLoadingProxies();
});

builder.Services.AddOpenApiDocument(options =>
{
    options.Title = "PromoCode Factory Giving To Customer API Doc";
    options.Version = "1.0";
});
builder.Services.AddMassTransitRabbitMqConfiguration(builder.Configuration);
builder.Services.AddMassTransit(configurator =>
{
    configurator.AddConsumer<PromoCodeConsumer>();
    configurator.UsingRabbitMq((context, configurator) =>
    {
        configurator.ConfigureEndpoints(context);
    });
});
var app = builder.Build();
if (app.Environment.IsDevelopment())
    app.UseDeveloperExceptionPage();
else
    app.UseHsts();

app.UseOpenApi();
app.UseSwaggerUi(x => { x.DocExpansion = "list"; });

app.UseHttpsRedirection();

app.UseRouting();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");
app.MigrateDatabase<DataContext>();
app.InitializeDatabase();
app.Run();