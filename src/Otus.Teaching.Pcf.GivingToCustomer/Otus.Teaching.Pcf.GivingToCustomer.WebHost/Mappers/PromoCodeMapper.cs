﻿using System;
using System.Collections.Generic;
using Otus.Teaching.Pcf.Common.Integration.Models;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;

public static class PromoCodeMapper
{
    public static PromoCode MapFromModel(GivePromoCodeToCustomerModel request, Preference preference,
        IEnumerable<Customer> customers)
    {
        var promoCode = new PromoCode
        {
            Id = request.PromoCodeId,
            PartnerId = request.PartnerId,
            Code = request.PromoCode,
            ServiceInfo = request.ServiceInfo,
            BeginDate = DateTime.Parse(request.BeginDate),
            EndDate = DateTime.Parse(request.EndDate),
            Preference = preference,
            PreferenceId = preference.Id,
            Customers = new List<PromoCodeCustomer>()
        };

        foreach (var item in customers)
            promoCode.Customers.Add(new PromoCodeCustomer
            {
                CustomerId = item.Id,
                Customer = item,
                PromoCodeId = promoCode.Id,
                PromoCode = promoCode
            });
        return promoCode;
    }
}