﻿using System.Threading.Tasks;
using Otus.Teaching.Pcf.Common.Integration.Models;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services.Base;

public interface IPromoCodeGivingService
{
    Task GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeToCustomerModel model, Preference preference);
}