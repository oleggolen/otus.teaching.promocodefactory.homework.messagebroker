﻿using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.Common.Integration.Models;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services.Base;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services;

public class PromoCodeGivingService : IPromoCodeGivingService
{
    private readonly IRepository<Customer> _customersRepository;
    private readonly IRepository<PromoCode> _promoCodesRepository;

    public PromoCodeGivingService(IRepository<Customer> customersRepository, IRepository<PromoCode> promoCodesRepository)
    {
        _customersRepository = customersRepository;
        _promoCodesRepository = promoCodesRepository;
    }

    public async Task GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeToCustomerModel model, Preference preference)
    {
        //  Получаем клиентов с этим предпочтением:
        var customers = await _customersRepository
            .GetWhere(d => d.Preferences.Any(x =>
                x.Preference.Id == preference.Id));

        var promoCode = PromoCodeMapper.MapFromModel(model, preference, customers);

        await _promoCodesRepository.AddAsync(promoCode);
    }
}