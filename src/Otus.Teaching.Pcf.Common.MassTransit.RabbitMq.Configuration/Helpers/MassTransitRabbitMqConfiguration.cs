﻿using MassTransit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Otus.Teaching.Pcf.Common.MassTransit.RabbitMq.Configuration.Helpers;

public static class MassTransitRabbitMqConfiguration
{
    public static IServiceCollection AddMassTransitRabbitMqConfiguration(this IServiceCollection serviceCollection, IConfigurationManager configurationManager)
    {
        serviceCollection.AddOptions<RabbitMqTransportOptions>().Configure(options =>
        {
            options.Host = configurationManager["RabbitMq:Host"];
            options.User = configurationManager["RabbitMq:User"];
            options.Pass = configurationManager["RabbitMq:Password"];
            options.Port = 5672;
            options.VHost = "/";
        });
        return serviceCollection;
    }
}